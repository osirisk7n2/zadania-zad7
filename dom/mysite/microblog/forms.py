__author__ = 'Ryujin'
from django import forms
from django.contrib.auth.models import User

class LoginForm(forms.Form):
    login=forms.CharField(required=True)
    password=forms.CharField(widget=forms.PasswordInput,required=True)


class RegiesterForm(forms.Form):
    login=forms.CharField(required=True)
    password=forms.CharField(widget=forms.PasswordInput,required=True)
    email=forms.EmailField(required=True)

class DodajForm(forms.Form):
    tresc=forms.CharField(required=True,widget=forms.Textarea)