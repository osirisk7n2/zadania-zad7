from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Wpis (models.Model):
    author= models.ForeignKey(User)
    content = models.TextField()
    date= models.DateTimeField()
    def __unicode__(self):
        return self.content