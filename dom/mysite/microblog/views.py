# Create your views here.
#p11
# tail -f var/log/apache2/error.log
from models import Wpis
from django.contrib.auth.models import  User
from django.core.exceptions import PermissionDenied, ObjectDoesNotExist
from django.contrib.auth import authenticate, login, logout,user_logged_in
import forms
from django.core.context_processors import csrf
import datetime
from django.shortcuts import get_object_or_404,get_list_or_404,render, HttpResponseRedirect, HttpResponse,redirect

def Index(request):
    if request.method=='GET':
        uz=User.objects.all()
        #wpisy = get_list_or_404(Wpis)
        wpisy=reversed(get_list_or_404(Wpis))
        return render(request,'microblog/index.html',{'wpisy':wpisy,'logged':request.user.is_authenticated(),'dodaj':forms.DodajForm()})
    elif request.method=='POST':
        user = request.user
        print user.id
        if forms.DodajForm(request.POST).is_valid():
            nowyWpis=Wpis(author=request.user,content=request.POST.get('tresc'),date=datetime.datetime.today())
            nowyWpis.save()
            wpisy=reversed(get_list_or_404(Wpis))
            return render(request,'microblog/index.html',{'wpisy':wpisy,'logged':request.user.is_authenticated(),'dodaj':forms.DodajForm()})
        else:
            wpisy=reversed(get_list_or_404(Wpis))
            return render(request,'microblog/index.html',{'wpisy':wpisy,'logged':request.user.is_authenticated(),'dodaj':forms.DodajForm(),'aht':"Podaj tresc!!"})




def Login(request):
    if request.method=="GET":
        if(request.user.is_authenticated()):
            wpisy=reversed(Wpis.objects.all())
            return render(request,'microblog/index.html',{'wpisy':wpisy,'logged':request.user.is_authenticated(),'dodaj':forms.DodajForm()})
        else:
            return render(request,'microblog/login.html',{'form':forms.LoginForm(),'ahtung':False})
    elif request.method=="POST":
       name= request.POST.get('login')
       password=request.POST.get('password')
       user= authenticate(username=name,password=password)

       wpisy=reversed(Wpis.objects.all())
       if user is not None:
           login(request,user)
           print "authenticated"
           #return user_logged_in
           return redirect('index')
           #return render(request,'microblog/index.html',{'wpisy':wpisy,'logged':request.user.is_authenticated(),'dodaj':forms.DodajForm()})
       else:
           print "not authenticated"
           return render(request,'microblog/login.html',{'form':forms.LoginForm,'ahtung':True})


def Register(request):
    if request.method=="GET":
        return render(request,'microblog/register.html',{'form':forms.RegiesterForm()})
    elif request.method=="POST":
        Rform = forms.RegiesterForm(request.POST)
        if Rform.is_valid():
            ulogin =Rform.cleaned_data['login']
            password=Rform.cleaned_data['password']
            email=Rform.cleaned_data['email']
            user = User.objects.create_user(username=ulogin,email=email,password=password)
            user.save()
            wpisy=get_list_or_404(Wpis)
            return HttpResponseRedirect('index.html',{'wpisy':wpisy,'logged':request.user.is_authenticated(),'dodaj':forms.DodajForm()})
        else:
            return render(request,'microblog/register.html',{'form':forms.RegiesterForm(),'ahtung':True,'dodaj':forms.DodajForm()})


def Logout(request):
    logout(request)
    wpisy=reversed(Wpis.objects.all())
    return render(request,'microblog/index.html',{'wpisy':wpisy,'logged':request.user.is_authenticated(),'dodaj':forms.DodajForm()})


def Delete (request, id):
    dWpis=Wpis.objects.get(id=id)
    if(dWpis.author==request.user):
        dWpis.delete()
        wpisy=reversed(Wpis.objects.all())
        return HttpResponse("Usunieto wpis! <a href=\"/~p11/mysite.wsgi/blog\">Wroc do indexu </a>")
    else:
        raise PermissionDenied


def Delete2 (request):
    if request.method=="POST":
        id=request.POST.get('id_form')
        dWpis=Wpis.objects.get(id=id)
        if(dWpis.author==request.user):
            dWpis.delete()
            wpisy=reversed(Wpis.objects.all())
            return HttpResponse("Usunieto wpis! <a href=\"/~p11/mysite.wsgi/blog\">Wroc do indexu </a>")
        else:
            raise PermissionDenied
    else:
        raise PermissionDenied


def Uonly(request):
    if request.method=='GET':
        uz=User.objects.all()
        #wpisy = get_list_or_404(Wpis)
        if request.user.is_authenticated():
            wpisy=reversed(Wpis.objects.all().filter(author=request.user))
        else:
            wpisy=None
        return render(request,'microblog/Uonly.html',{'wpisy':wpisy,'logged':request.user.is_authenticated(),'dodaj':forms.DodajForm()})
    elif request.method=='POST':
        user = request.user
        print user.id
        if forms.DodajForm(request.POST).is_valid():
            nowyWpis=Wpis(author=request.user,content=request.POST.get('tresc'),date=datetime.datetime.today())
            nowyWpis.save()
            wpisy=reversed(get_list_or_404(Wpis))
            return render(request,'microblog/Uonly.html',{'wpisy':wpisy,'logged':request.user.is_authenticated(),'dodaj':forms.DodajForm()})
        else:
            wpisy=reversed(get_list_or_404(Wpis))
            return render(request,'microblog/Uonly.html',{'wpisy':wpisy,'logged':request.user.is_authenticated(),'dodaj':forms.DodajForm(),'aht':"Podaj tresc!!"})

