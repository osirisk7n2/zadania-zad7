from django.conf.urls import patterns, url
from django.views.generic import TemplateView
from views import *

urlpatterns = patterns('',
    url(r'^$', Index, name='index'),
    url(r'^delete/(\d+)/$',Delete, name='delete'),
    url(r'^login/',Login, name='login'),
    url(r'^logout/',Logout, name='logout'),
    url(r'^register/',Register, name='register'),
    url(r'^Uonly/delete2/',Delete2, name='delete2'),
    url(r'^Uonly/',Uonly, name='Uonly'),

)
    #url(r'^$', 'microblog.views.Index'),
    #url(r'login', 'microblog.views.Login'),
    #url(r'register', 'microblog.views.Register'),
    #url(r'index', 'microblog.views.Index'),
    #url(r'logout', 'microblog.views.Logout'),
    #url(r'delete/(\d+)/$','microblog.views.Delete'),
    #url(r'Uonly', 'microblog.views.Uonly'),